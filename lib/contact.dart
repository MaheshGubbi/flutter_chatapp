
import 'package:firebase/home_page.dart';
import 'package:firebase/msg.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';



class Contact extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Contact> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String userId;
  String OwnerName;

  List<Item> UsersList = List();
  Item item;
  DatabaseReference itemRef;
  DatabaseReference databaseId;

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController controller = new TextEditingController();
  TextEditingController controller2 = new TextEditingController();

  @override
    void initState() {
    super.initState();
    _init();
    uName();
    item = Item("", "");
    final FirebaseDatabase database = FirebaseDatabase.instance;
    //Rather then just writing FirebaseDatabase(), get the instance.
    itemRef = database.reference().child('User');
    itemRef.onChildAdded.listen(_onEntryAdded);
    }

  Future _init() async {
    await FirebaseDatabase.instance.setPersistenceEnabled(true);

  }

  void uName() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    userId = user.uid;
    Object _objdatabase;
    // single value retrieve
    await FirebaseDatabase.instance.reference().child("account").child("$userId").child("details").child("name").once().then((
        DataSnapshot snapshot) {
      _objdatabase = snapshot.value;
      OwnerName = _objdatabase;
      print("owner : $OwnerName");
      });
    //return OwnerName;
  }

  _onEntryAdded(Event event) {
    setState(() {
      UsersList.add(Item.fromSnapshot(event.snapshot));
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Contacts List'),
        actions: <Widget>[

          IconButton(
            icon: Icon(Icons.add),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => new HomePage(),
                ),
              );
            },
          ),

          PopupMenuButton<String>(
            onSelected: (String value){},
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(child: Text('Settings')),
                PopupMenuItem(child: Text('Logout'))

              ];
            },
          ),
        ],
      ),
      resizeToAvoidBottomPadding: false,
      body: Column(
        children: <Widget>[
          Flexible(
              child: FirebaseAnimatedList(
                query: itemRef,
                itemBuilder: (BuildContext context, DataSnapshot snapshot,
                    Animation<double> animation, int index) {
                  return new ListTile(
                    leading: Icon(Icons.person),
                    title: Text(UsersList[index].name,style: TextStyle(fontSize: 18.0),),

                    onTap: () {
                      Scaffold.of(context).showSnackBar(
                          SnackBar(content: Text('Clicked on ${UsersList[index].name}')));
                      String toMessage = UsersList[index].name;
                      print("$toMessage");
                      print("user : ${UsersList[index].name}");
                      String dash = "_${UsersList[index].name}";
                     String meANDyou = "$OwnerName$dash";
                     print("con : $meANDyou");
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new messageHere(value2: meANDyou, value1: toMessage),
                        ),
                      );
                    },
                    onLongPress: (){
                    //  itemRef.onChildRemoved.listen(_onEntryRemoved);
                    },
                  );
                },
              ),
          ),
        ],
      ),
    );
  }



  void nameId() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String userId = user.uid;
    databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$userId");
    }
}
//****** List of item *******\\
class Item {
  String key;
  String name;
  String Number;

  Item(this.name, this.Number);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        name = snapshot.value["name"],
        Number = snapshot.value["Number"];

  toJson() {
    return {
      "name": name,
      "Number": Number,
    };
  }
}



