import 'package:firebase/auth_provider.dart';
import 'package:firebase/contact.dart';
import 'package:firebase/main.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';
final auth = FirebaseAuth.instance;

bool _progressBarActive = false;


//*******  name validation *******\\
class _name {
  static String validate(String value1) {
    String value = value1.trim();
    if(value == null || value.isEmpty)
      return 'Enter your Name';
    else if (value.length < 3)
      return 'Name must be more than 3 charater';
    else
      return null;
  }
}
//*******  Number validation *******\\
class _number {
  static String validate(String value1) {
    String value = value1.trim();

    if(value == null || value.isEmpty)
      return 'Enter your Number';
    else if (value.length < 10)
      return 'Enter valid Mobile Number';
    else
      return null;
  }
}

class HomePage extends StatelessWidget {

  HomePage({this.onSignedOut});
  final VoidCallback onSignedOut;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'chat Demo',
     // theme: ThemeData.dark(),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: home(),
    );
  }
}


class home extends StatefulWidget {
  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {

  TextEditingController controller = new TextEditingController();

  List<Item> Users = List();
  Item item;

  DatabaseReference itemRef;
  DatabaseReference myRef;
  DatabaseReference nameRef;
  DatabaseReference databaseId;
  bool _isInAsyncCall = false;
  String N;
  String person_name;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final FirebaseDatabase database = FirebaseDatabase.instance; //Rather then just writing FirebaseDatabase(), get the instance.


  @override
  void initState() {
    super.initState();
    item = Item("", "");
     //itemRef = database.reference().child('User');
     itemRef = database.reference().child('accounts');
  }

  void scaffoldContext(){
   new Container(width: 0.0, height: 0.0);
  }

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {

      setState(() {
        _isInAsyncCall = true;
      });

      Future.delayed(Duration(seconds: 2), ()
      {
        setState(() {
          nameId();
          handleSubmit();
          _isInAsyncCall = false;

          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Contact()),
          );
        });
        });


    }
  }

  //******* To database submitting  *******\\
  void handleSubmit() {

    String username = controller.text;
    final FormState form = formKey.currentState;

    if (form.validate()) {

        form.save();
        form.reset();
        myRef = database.reference().child('User').child(username);

        myRef.set(item.toJson());
        _isInAsyncCall = false;
        Fluttertoast.showToast(msg: "Data Successfully Saved ",
          backgroundColor: Colors.black26,
          textColor: Colors.green,
          gravity: ToastGravity.BOTTOM,
        );
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat App'),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(child: Text('Settings')),
                PopupMenuItem(child: Text('Logout'),
                )
              ];
            },
          ),
        ],
      ),
      resizeToAvoidBottomPadding: false,
      body:
      ModalProgressHUD(
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(16.0),
            child: buildLoginForm(context),
          ),
        ),
        inAsyncCall: _isInAsyncCall,
        // demo of some additional parameters
        opacity: 0.5,
        progressIndicator: CircularProgressIndicator(),
      ),



    );
  }

  Widget buildLoginForm(BuildContext context) {
    return Form( child :
      Column(
        children: <Widget>[
          Flexible(
            flex: 0,
            child: Padding(
              padding: EdgeInsetsDirectional.fromSTEB(0.0, 50.0, 0.0, 0.0),
              child: Center(
                child: Form(
                  key: formKey,
                  child: Flex(
                    direction: Axis.vertical,
                    children: <Widget>[
                      /*new Container(width: 60.0, height: 20.0, //color: Colors.lightGreen,
                        child: new CircularProgressIndicator(),),
*/
                      Text("Profile",style: TextStyle(fontSize: 30.0,color : Colors.lightBlue),),
                      ListTile(
                        //leading: Icon(Icons.info),
                        title:
                        TextFormField(
                          controller: controller,
                          keyboardType: TextInputType.text,
                          //controller: person,
                          decoration: InputDecoration(labelText: 'Name'),
                          //initialValue: 'Enter your name',
                          validator: _name.validate,
                          onSaved: (val) => item.name = val,
                          //validator: (val) => val == "" ? val : null,
                        ),
                      ),

                      ListTile(
                        title: TextFormField(
                          decoration: InputDecoration(labelText: 'Number',),
                          initialValue: "",
                          keyboardType: TextInputType.number,
                          textInputAction: TextInputAction.done,
                          validator: _number.validate,
                          onSaved: (val) => item.Number = val,

                        ),
                      ),
                      _progressBarActive == true? CircularProgressIndicator(): RaisedButton(
                          key: Key('SaveData'),
                          child: Text('Save', style: TextStyle(fontSize: 20.0)),
                          onPressed: (){
                            validateAndSubmit();
                          } ),
                      RaisedButton(
                        child: Text('New Message', style: TextStyle(fontSize: 20.0)),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Contact()),
                          );
                        },
                      ),

                      RaisedButton(
                        child: Text('Log Out', style: TextStyle(fontSize: 20.0)),
                        onPressed: () {
                          _signOut();
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => MyApp()),
                          );
                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),

    );
  }


  //******* user ID retrieve  *******\\
  void nameId() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String userId = user.uid;
    databaseId = FirebaseDatabase.instance.reference().child("account").child("$userId").child("details");
    databaseId.set(item.tooJson());

  }
}

//******* logout  *******\\
Future _signOut() async {
  await auth.signOut();
  Fluttertoast.showToast(msg: "Successfully Logout ",
      backgroundColor: Colors.transparent,
      textColor: Colors.green);
}

//*******  firebase database values *******\\
class Item {
  String key;
  String name;
  String Number;

  Item(this.name, this.Number);

  Item.fromSnapshot(DataSnapshot snapshot)
      : name = snapshot.value["name"],
        Number = snapshot.value["body"];

  //******* firebase value Retrieve  *******\\
  toJson() {
    return {

      "name": name,
      "Number": Number,

    };
  }
  tooJson() {
    return {

      "name": name,
      "Number": Number,


    };
  }




}


