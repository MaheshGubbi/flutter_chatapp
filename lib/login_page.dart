import 'package:firebase/auth_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class EmailFieldValidator {
  static String validate(String value) {
    return value.isEmpty ? 'Email can\'t be empty' : null;
  }
}
class PasswordFieldValidator {
  static String validate(String value) {
    return value.isEmpty ? 'Password can\'t be empty' : null;
  }
}
class LoginPage extends StatefulWidget {
  LoginPage({this.onSignedIn});
  final VoidCallback onSignedIn;

  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

enum FormType {
  login,
  register,
}

final auth = FirebaseAuth.instance;
class _LoginPageState extends State<LoginPage> {
  final formKey = GlobalKey<FormState>();
  bool _isInAsyncCall = false;
  bool _isInvalidAsyncUser = false;

  String _email;
  String _password;
  FormType _formType = FormType.login;

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }
  //******* validation and error exception handling  *******\\
  void validateAndSubmit()async {

    if (validateAndSave()) {
//******* Error exception handling  *******\\
      try {
        var auth = AuthProvider
            .of(context)
            .auth;
        if (_formType == FormType.login) {
          String userId =
          await auth.signInWithEmailAndPassword(_email, _password);
          _isInAsyncCall = true;
          Future.delayed(Duration(seconds: 2), () {
            print('Successfully Login : $userId');
            Fluttertoast.showToast(msg: "Successfull Login..",
              backgroundColor: Colors.transparent,
              textColor: Colors.green,
              gravity: ToastGravity.BOTTOM,
            );
            _isInAsyncCall = false;
          });
        } else {
          String userId = await auth
              .createUserWithEmailAndPassword(_email, _password);

          Fluttertoast.showToast(msg: "Successfully created Account");
          final DatabaseReference database = FirebaseDatabase.instance
              .reference().child("account").child("$userId").child("Acount");
          _isInAsyncCall = false;
          {
            database.set({
              'password': '$_password',
              'username': '$_email'
            });
          }
        }
        widget.onSignedIn();
        _isInAsyncCall = false;
      } catch (e) {

        switch (e.message) {
          case 'There is no user record corresponding to this identifier. The user may have been deleted.':
            Fluttertoast.showToast(
                msg: "There is no user record corresponding to this identifier",
                gravity: ToastGravity.CENTER,
                backgroundColor: Colors.transparent,
                textColor: Colors.black);
            _isInAsyncCall = false;
            break;
          default:


            Fluttertoast.showToast(msg: " ${e.message}",
                timeInSecForIos: 5000,
                gravity: ToastGravity.CENTER,
                backgroundColor: Colors.transparent,
                textColor: Colors.redAccent);
            _isInAsyncCall = false;

            break;
        }
        _isInAsyncCall = false;
      }
      _isInAsyncCall = false;
    }
    _isInAsyncCall = false;
  }
//*******  Pass to registration function *******\\
  void moveToRegister() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.register;
    });
  }
//******* Pass to Login function *******\\
  void moveToLogin() {

    formKey.currentState.reset();
    setState(() {
      _formType = FormType.login;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        body: ModalProgressHUD(
          child: SingleChildScrollView(
            child:
            Container(
                padding: EdgeInsets.all(16.0),
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: buildInputs() + buildSubmitButtons(),
                  ),
                )),
          ),
          inAsyncCall: _isInAsyncCall,
          // demo of some additional parameters
          opacity: 0.5,
          progressIndicator: CircularProgressIndicator(),
        ),


      );
  }
  //******* Email and passwordvTextfild   *******\\
  List<Widget> buildInputs() {
    return [
         Padding(
           padding: const EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 60.0),
           child: new Center(
            child: Text("Welcome",style: TextStyle(fontSize: 30.0,color : Colors.lightBlue),),
           ),
         ),

      TextFormField(
        key: Key('email'),
        decoration: InputDecoration(labelText: 'Email'),
        validator: EmailFieldValidator.validate,
        onSaved: (value) => _email = value,
      ),
      TextFormField(
        key: Key('password'),
        decoration: InputDecoration(labelText: 'Password'),
        obscureText: true,
        validator: PasswordFieldValidator.validate,
        onSaved: (value) => _password = value,
      ),
    ];
  }

//******* Data submitting buttons *******\\

  List<Widget> buildSubmitButtons() {
    if (_formType == FormType.login) {
      return [
        RaisedButton(
          key: Key('signIn'),
          child: Text('Login', style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold,color: Colors.black)),
          onPressed:() {
              validateAndSubmit();

          }
        ),
        FlatButton(
          child: Text('Create a new Account',
              style: TextStyle(fontSize: 20.0)),
          onPressed: moveToRegister,

        ),
      ];
    } else {
      return [
        RaisedButton(
          child: Text('Create an account',
              style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold,color: Colors.black)),
          onPressed: validateAndSubmit,
        ),
        FlatButton(
          child: Text('Have an account? Login',
              style: TextStyle(fontSize: 20.0,)),
          onPressed:(){ //
          moveToLogin();
          }
        ),
      ];
    }
  }
}
