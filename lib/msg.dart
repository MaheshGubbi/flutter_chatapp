 import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

 final ThemeData iOSTheme = new ThemeData(
   primarySwatch: Colors.red,
   primaryColor: Colors.grey[400],
   primaryColorBrightness: Brightness.dark,
 );

 final ThemeData androidTheme = new ThemeData(
   primarySwatch: Colors.blue,
   accentColor: Colors.green,
 );

 String defaultUserName;

class messageHere extends StatefulWidget {
  final String value1;
  final String value2;

  messageHere({Key key, this.value1, this.value2}) : super(key: key);

  @override
  _messageHereState createState() => _messageHereState();
}
 class _messageHereState extends State<messageHere> {
   String user_Name;
   String userId;
   String isMe;
   String meANDyou;
   String me;
   int test = 40;
   DatabaseReference databaseId;
   DatabaseReference itemRef;
   DatabaseReference databaseMsg;
   DatabaseReference databaseId1;
   final TextEditingController _textController = new TextEditingController();
   bool _isWriting = false;


   List<Item> UsersList = List();
   Item item;

   @override
   void initState() {
     super.initState();
     _init();
     item = Item("", "");
     print("received :${widget.value1}") ;
     me = "${widget.value2}";
     print(me);

     databaseMsg = FirebaseDatabase.instance.reference().child("messagalu").child("$me");
     final FirebaseDatabase database = FirebaseDatabase.instance; //Rather then just writing FirebaseDatabase(), get the instance.
     itemRef = database.reference().child('User');
     itemRef.onChildAdded.listen(_onEntryAdded);
     itemRef.onChildRemoved.listen(_onEntryRemoved);
     itemRef.onChildChanged.listen(_onEntryChanged);
   }


   Future _init() async {
     await FirebaseDatabase.instance.setPersistenceEnabled(true);
     Object _objdatabase;
     FirebaseUser user = await FirebaseAuth.instance.currentUser();
     userId = user.uid;
     print("user Id nnn $userId");
     await FirebaseDatabase.instance.reference().child("account").child("$userId").child("details").child("name").once().then((
         DataSnapshot snapshot) {
       _objdatabase = snapshot.value;
       user_Name = _objdatabase;
       defaultUserName =user_Name;

       String dash = "_${widget.value1}";
       meANDyou = "$user_Name$dash";
       isMe = "$user_Name";
       databaseMsg = FirebaseDatabase.instance.reference().child("messagalu").child("$meANDyou");
       // print("name : $OwnerName");
     });

   }

   _onEntryAdded(Event event) {
     setState(() {
       UsersList.add(Item.fromSnapshot(event.snapshot));
     });
   }

   _onEntryChanged(Event event) {
     var old = UsersList.singleWhere((entry) {
       return entry.key == event.snapshot.key;
     });
     setState(() {
       UsersList[UsersList.indexOf(old)] = Item.fromSnapshot(event.snapshot);
     });
   }

   _onEntryRemoved(Event event) {
     setState(() {
       UsersList.remove(Item.fromSnapshot(event.snapshot));
     });
   }


   @override
   Widget build(BuildContext ctx) {
     final bg = Colors.greenAccent.shade100;
     //final icon = send1 ? Icons.done_all : Icons.done;
     //final bg = isMe ? Colors.white : Colors.greenAccent.shade100;

    //******* separate sender and receiver  messages*******\\
     /*final radius = isMe
         ? BorderRadius.only(
       topRight: Radius.circular(5.0),
       bottomLeft: Radius.circular(10.0),
       bottomRight: Radius.circular(5.0),
     )
         : BorderRadius.only(
       topLeft: Radius.circular(5.0),
       bottomLeft: Radius.circular(5.0),
       bottomRight: Radius.circular(10.0),
     );*/

     return new Scaffold(
       appBar: new AppBar(
         title: new Text("${widget.value1}"),
         elevation:
         Theme.of(ctx).platform == TargetPlatform.iOS ? 0.0 : 6.0,
       ),

         body: new Container(
           child: new Column(
             children: <Widget>[
            //   new Text("Testing ${widget.value2}",style: TextStyle(fontSize: 20.0),),

           Flexible(
             child: FirebaseAnimatedList(
               sort: (DataSnapshot a, DataSnapshot b) =>
                   b.key.compareTo(a.key),
             query: databaseMsg,

             padding: new EdgeInsets.all(10.0),
             reverse: true,
               itemBuilder: (context, snapshot, animation, index) {
                 return  new Container(
                   decoration: BoxDecoration(
                     boxShadow: [
                       BoxShadow(
                           blurRadius: .5,
                           spreadRadius: 1.0,
                           color: Colors.black.withOpacity(.12))
                     ],
                     color: bg,
                     borderRadius: new BorderRadius.circular(25.0),
                   ),
                   margin: const EdgeInsets.symmetric(vertical: 5.0),
                   child: Row(

                      crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         new Container(
                           margin: const EdgeInsets.only(right: 16.0),
                           child: new CircleAvatar(
                             child: new Image.network("http://res.cloudinary.com/kennyy/image/upload/v1531317427/avatar_z1rc6f.png"),
                           ),
                         ),

                         new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                         children: <Widget>[
                           //Bubble(Text("ok")),
                         new Text(snapshot.value['sender'],style: new TextStyle(
                             color: Colors.blueAccent,
                             fontSize: 18.0,
                             fontWeight: FontWeight.w900)),
                         new Container(
                         child: new Text(snapshot.value['message'],style: new TextStyle(
                             color: Colors.black,
                             fontSize: 16.0,
                             fontWeight: FontWeight.w900)),
                         ),
                         ],
                         ),
                   ],
                   ),
                 );
               },
           ),
           ),

          // new Text("save ${widget.value2}"),
               new Divider(height: 1.0),

               new Container(
                 child: _buildComposer(),

                 decoration: new BoxDecoration(color: Theme.of(ctx).cardColor),

               ),
             ],
           ),
           decoration: Theme.of(context).platform == TargetPlatform.iOS
               ? new BoxDecoration(
               border: new Border(
                   top: new BorderSide(
                     color: Colors.grey[200],
                   )))
               : null,
         )
     );
   }


   Widget _buildComposer() {
     return new IconTheme(
       data: new IconThemeData(color: Theme.of(context).accentColor),
       child: new Container(
           margin: const EdgeInsets.symmetric(horizontal: 9.0),
           child: new Row(
             children: <Widget>[
               new Flexible(
                 child: new TextField(
                   controller: _textController,
                   onChanged: (String txt) {
                     setState(() {
                       _isWriting = txt.length > 0;
                     });
                   },
                   onSubmitted: _submitMsg,
                   decoration:
                   new InputDecoration.collapsed(hintText: "Write a Message"),
                 ),
               ),
               new Container(
                   margin: new EdgeInsets.symmetric(horizontal: 3.0),
                   child: Theme.of(context).platform == TargetPlatform.iOS
                       ? new CupertinoButton(
                       child: new Text("Submit"),
                       onPressed: _isWriting ? () => _submitMsg(_textController.text)
                           : null
                   )
                       : new IconButton(
                     icon: new Icon(Icons.send),
                     onPressed: _isWriting
                         ? () => _submitMsg(_textController.text)
                         : null,
                   )
               ),
             ],
           ),
           decoration: Theme.of(context).platform == TargetPlatform.iOS
               ? new BoxDecoration(
               border:
               new Border(top: new BorderSide(color: Colors.brown))):null,
       ),
     );
   }

   void _submitMsg(String text) {
     uName();
     _sendMessage(sender:user_Name, messageText: text, imageUrl: null);

     _textController.clear();
     setState(() {
       _isWriting = false;
     });
   }

   void _sendMessage({ String sender,String messageText, String imageUrl}) {
     databaseId.push().set({
       'message': messageText,
       'sender' : sender
     });
     databaseId1.push().set({
       'message': messageText,
       'sender' : sender
     });
     }

// retrieve user ID
   void nameId() async {
     FirebaseUser user = await FirebaseAuth.instance.currentUser();
     userId = user.uid;
     databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$userId");

   }

   void uName() async {
     FirebaseUser user = await FirebaseAuth.instance.currentUser();
     userId = user.uid;
     Object _objdatabase;
     // single value retrieve
     await FirebaseDatabase.instance.reference().child("account").child("$userId").child("details").child("name").once().then((
         DataSnapshot snapshot) {

       _objdatabase = snapshot.value;
       user_Name = _objdatabase;
       defaultUserName =user_Name;
       String dash = "_${widget.value1}";
       String dash1 = "_$user_Name";


       meANDyou="$user_Name$dash";
       String youANDme="${widget.value1}$dash1";
       databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$meANDyou");
       databaseId1 = FirebaseDatabase.instance.reference().child("messagalu").child("$youANDme");
       });
   }

  getSentMessageLayout(DataSnapshot messageSnapshot) {

  }

  getReceivedMessageLayout(DataSnapshot messageSnapshot) {

  }

  sender() {}

  receiver() {}

 }
 class myData {
   String name, message, profession;

   myData(this.name, this.message, this.profession);
 }

 class Item {
   String key;
   String sender;
   String message;

   Item(this.sender, this.message);

   Item.fromSnapshot(DataSnapshot snapshot)
       : key = snapshot.key,
         sender = snapshot.value["sender"],
         message = snapshot.value["message"];

   toJson() {
     return {
       "sender": sender,
       "message": message,
     };
   }
 }
