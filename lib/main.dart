

import 'package:firebase/Athentication.dart';
import 'package:firebase/auth_provider.dart';
import 'package:firebase/Page_Root.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AuthProvider(
      auth: Auth(),
      child: MaterialApp(
        title: 'Flutter login demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        //******* direction to root page *******\\
        home: RootPage(),
      ),
    );
  }
}
